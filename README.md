# Calculator Web App

An example project for a GitLab pipeline.

## Requirements

- Python >= 3.5

## Running tests

Install requirements:

```
pip install -r requirements.txt
```

Run all tests:

```
python -m unittest
```

## Running the production server

Install requirements:

```
pip install -r requirements.txt
```

Run the server:

```
gunicorn --bind 0.0.0.0:5000 wsgi:app
```

## Running with Docker

Build the image:

```
docker build -t calculator .
```

Run it:

```
docker run -d -p 9000:5000 --name my-calculator calculator
```
